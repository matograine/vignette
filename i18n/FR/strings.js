    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "ERREUR : Le titre ou la clef publique n'est pas renseigné !";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `ATTENTION ! Vous avez entré la clef publique de viĞnette : ${creator_pubkey}. Est-ce bien ce que vous voulez faire ?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `La clef publique ${pubkey} n'est pas valide`;
    const Function_STRING_ERROR_WRONG_CHECKSUM    = (checksum, pubkey) => `Le checksum ${checksum} ne correspond pas à la clef publique ${pubkey}. Veuillez vérifier la clef publique.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "ERREUR : Votre navigateur n'est pas supporté."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `ERREUR : la clef publique fait moins de ${min_length} caractères.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `ERREUR : la clef publique fait plus de ${max_length} caractères.\n`
    const Function_STRING_SEND_DONATION               = (pubkey_ck) => `Pour faire un don à Viğnette, envoyez la monnaie à la clef publique : ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\nCette clef publique a été copiée dans votre presse-papier. Vous pouvez la coller directement dans Cesium."