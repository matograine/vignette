    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "ERROR: Title or public key are not given !";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `BE CAREFUL! you entered the public key of viÄŸnette: ${creator_pubkey}. Is that what you want to do?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `Public key ${pubkey} is not valid.`;
    const Function_STRING_ERROR_WRONG_CHECKSUM = (checksum, pubkey) => `The checksum ${checksum} does not match the public key ${pubkey}. Please double-check the public key.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "ERROR: Your browser is not supported."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `ERROR: The public key is less than ${min_length} characters long.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `ERROR: The public key is more than ${max_length} characters long.\n`
    const Function_STRING_SEND_DONATION               = (pubkey_ck) => `To make a donation to Viğnette, please send Ğ1 on public key : ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\nThis public key has been copied in the clipboard. You can paste it directly in your wallet software."
