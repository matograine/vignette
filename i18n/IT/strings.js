    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "ERRORE: Il titolo o la chiave pubblica non è indicato!";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `ATTENZIONE! Hai inserito la chiave pubblica di ViĞnetta : ${creator_pubkey}. È quello che vuoi fare?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `La chiave pubblica ${pubkey} non è valida`;
    const Function_STRING_ERROR_WRONG_CHECKSUM    = (checksum, pubkey) => `Il checksum ${checksum} non corrisponde alla chiave pubblica  ${pubkey}. Pregasi verificare la chiave pubblica.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "ERRORE : Il tuo browser non è supportato."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `ERRORE : la chiave pubblica ha meno ${min_length} caratteri.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `ERREUR : la chiave pubblica ha più di ${max_length} caratteri.\n`
    const Function_STRING_SEND_DONATION               = (pubkey_ck) => `Per fare una donazione a Viğnetta, invia la moneta sulla chiave pubblica: ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\n Questa chiave pubblica è stata copiata nei tuoi apppunti. Puoi incollarla direttamente in Cesium."