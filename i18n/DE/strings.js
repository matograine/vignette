    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "FEHLER: Der Name oder öffentliche Schlüssel wurde nicht angegeben!";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `ACHTUNG! Sie haben den öffentlichen Schlüssel von ViĞnette angegeben: ${creator_pubkey}. Sind Sie sich sicher?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `Der öffentliche Schlüssel ${pubkey} ist nicht gültig`;
    const Function_STRING_ERROR_WRONG_CHECKSUM    = (checksum, pubkey) => `Die Kontrolsumme ${checksum} stimmt nicht mit dem öffentlichen Schlüssel ${pubkey} überein. Bitte überprüfen Sie den öffentlichen Schlüssel.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "FEHLER: Ihr Browser wird nicht unterstützt."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `FEHLER: Der öffentliche Schlüssel besteht aus weniger als ${min_length} Zeichen.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `FEHLER: Der öffentliche Schlüssel besteht aus mehr als ${max_length} Zeichen.\n`
    const Function_STRING_SEND_DONATION               = (pubkey_ck) => `Um Viğnette zu unterstützen, spenden Sie bitte an den öffentlichen Schlüssel: ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\nDieser öffentliche Schlüssel wurde in Ihren Zwischenspeicher kopiert. Sie können ihn nun direkt in Cesium einfügen."
