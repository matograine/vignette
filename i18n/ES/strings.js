    //---- Error and message strings for JS code

    const STRING_ERROR_TITLE_PUBKEY_NOT_PROVIDED  = "ERROR: ¡No se ha especificado el título o la clave pública!";
    const Function_STRING_WARNING_SAME_PUBKEY     = (creator_pubkey) => `¡ALERTA! Us. ha introcido la clave pública de viĞnette: ${creator_pubkey}. ¿Es eso lo que quiera­a hacer?`
    const Function_STRING_ERROR_INVALID_PUBKEY    = (pubkey) => `La clave pública ${pubkey} no esta validada`;
    const Function_STRING_ERROR_WRONG_CHECKSUM    = (checksum, pubkey) => `El checksum ${checksum} no corresponde a la clave pública ${pubkey}. Por favor, compruebe la clave pública.`
    const STRING_ERROR_BROWSER_NOT_SUPPORTED      = "ERROR: Su navegador no esta soportado."
    const Function_STRING_ERROR_PUBKEY_TOO_SHORT  = (min_length) => `ERROR: la clave pública tiene menos de ${min_length} caracteres.\n`
    const Function_STRING_ERROR_PUBKEY_TOO_LONG   = (max_length) => `ERROR: la clave pública tiene más de ${max_length} caracteres.\n`
    const Function_STRING_SEND_DONATION           = (pubkey_ck) => `Para hacer un donativo a ViĞnette, puede hacerlo a la clave pública: ${pubkey_ck}.`
    const STRING_PUBKEY_COPIED_IN_CLIPBOARD       = "\n\nEsta clave pública se ha copiado en su portapapeles. Ahora puede engancharla directamente en Cesium."
