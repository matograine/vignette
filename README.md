# ViĞnette

ViĞnette est un petit site qui permet aux usagers de créer des affichettes pour les Ğmarchés.
Il permet également de créer des planches de cartes de visite.
Ces affichettes indiquent :
* un titre
* une description
* une photo (ou un symbole Ğ)
* la clef publique avec son checksum
* le QR-code de la clef publique

## Installation

Pour installer ViĞnette, simplement décompresser l'archive sur son serveur.
Vous pouvez modifier des options du site (langue par défaut, orientation de l'URL sur les vignettes) en faisant le build vous-même.

## Release

Pour construire un site ViĞnette avec ses traductions, on exécute le script de build:

`python3 build.py`

On peut lui passer de zéro à deux arguments :

`python3 build.py <url_orientation> <default_lang>`

- `url_orientation` peut être "horizontal", "vertical" ou "none". La valeur par défaut est "horizontal". Il définit l'orientation de l'URL sur les ViGnettes.
- `default_lang` va automatiquement créer une page par défaut dans la langue indiquée. Il doit correspondre à une des langues présentes dans i18n.

### Images

Les images d'exemples ne sont pas inclues dans l'historique git pour ne pas surcharger les dépôts.
Vous pouvez les récupérer :
```
# A la racine du dépôt

wget g1cotis.fr/vignette/img_vignette.zip
unzip img_vignette.zip
cp -r img_vignette/* i18n/
```

Lancez le build ensuite.

## Traduction

Les fichiers de traduction doivent se trouver dans `i18n` suivant ce schéma:
```
i18n
|- <lang_code>
  |- html_translations.json (translation strings for HTML files)
  |- strings.js (error messages)
  |- logo_g.png (translated logo, size 900x900)
  |- example_vignette.png (example vignette, size 850x607px) (example vignette, size 850x607px) 
```
Notez que example_vignette.png est optionnel, et n'est pas inclus dans l'historique Git pour éviter du poids inutile.
Si vous créer des exemples traduits, veuillez l'indiquer à Matograine dans la MR, et lui envoyer l'image.

Le nom du dossier (FR, CAT, etc) est utilisé dans l'interface, veuillez utiliser des codes explicites.

Si vous voulez ne pas avoir certaines traducations, supprimez le dossier correspondant avant de lancer le build.


[EN]

ViĞnette is a web utility that creates posters for Ğ1 currency.
It can also be used to create business cards.
Posters / cards will display :
- a title
- a description 
- a photo (or Ğ symbol)
- the public keywith the checksum (or a short form on business cards)
- the QR-code of the public key

To install ViĞnette, simply clone the repo (or uncompress the release archive) anywhere on your web server. It is all static files. You can change options of the website (default language, orientation of URL on the posters) by launching the build yourself.

## Release

To build the site with the translations :

`python3 build.py`

It takes zero to two arguments:

`python3 build.py <url_orientation> <default_lang>`

- `url_orientation` can be "horizontal", "vertical" or "none". Default is "horizontal". It rules the orientation of URL on vignettes.
- `default_lang` will automatically create a homepage in given language. It must match one lang existing in i18n.

### Images

Example images are not part of the Git history to avoid overloading repos.
You can fetch them:
```
# At the repo root

wget g1cotis.fr/vignette/img_vignette.zip
unzip img_vignette.zip
cp -rf img_vignette/* i18n/
```
Launch the build script afterwards.

## Translation

Translation files must be located in dir `i18n` and follow this schema:
```
i18n
|- <lang_code>
  |- html_translations.json (translation strings for HTML files)
  |- strings.js (error messages)
  |- logo_g.png (translated logo, size 900x900px)
  |- example_vignette.png (example vignette, size 850x607px) 
```
Note that example_vignette.png is optional and is not included in Git tree to avoid useless weight.
If you create a translated example_vignette.png, please notify it in the MR and send it to Matograine.

The name of the lang dir (FR, CA, etc) is used in the interface, please do not use fuzzy dir names.

You may delete a translation dir before launching the build if you don't want to use it.
